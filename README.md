# Example Code for Scientific Data publication
# A dataset of clinically recorded radar vital signs with synchronised reference sensor signals

In this repository you can find the entire code used for the technical validation shown in https://www.nature.com/articles/s41597-020-00629-5. Furthermore, a script for viewing the data is also included in the repository which can easily be used by configuring the subject ID and scenarios which shall be viewed. The code was written and tested using MATLAB R2020a for Microsoft Windows.

Link to the figshare database: https://doi.org/10.6084/m9.figshare.12186516.v2
(Direct downloadlink of the database: https://ndownloader.figshare.com/articles/12186516/versions/2)

Contact: sven.schellenberger(at)tuhh.de